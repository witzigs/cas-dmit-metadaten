+++
weight = 230
+++
{{% section %}}

### MODS

---

#### Intro
* **M**etadata **O**bject **D**escription **S**tandard
* Kompromiss zwischen MARC (komplex) und Dublin Core (oft zu einfach)
* Repositories, Digitale Sammlungen
* Häufig zusammen mit METS verwendet

<img class="special-img-class" style="width:40%;border:none" src="/images/mods.gif" />

---

#### Geschichte und Entwicklung
* Entwicklung 2003
* LoC, MARC-Office und weitere Experten
* Ziel: Standard für reichhaltigere Beschreibungen in XML
* Semantik basiert auf MARC21
* MODS enthält ein Subset von MARC-Elementen

---

#### Struktur
* XML tags auf Englisch (statt Feldnummern von MARC)
* Verschachtelte Struktur
* 20 Top level elements, sub elements
* Attribut type für Spezifizierung

---

#### Dokumentation und Beispiele
* [MODS Official Web Site](https://www.loc.gov/standards/mods/)
* [MODS Elements and Attributes](https://www.loc.gov/standards/mods/userguide/generalapp.html)
* [Beispiel aus Serval (Oberfläche)](https://serval.unil.ch/en/notice/serval:BIB_BE27CB42DEC8)
* [Beispiel aus Serval (OAI)](https://serval.unil.ch/oaiprovider?verb=GetRecord&metadataPrefix=mods&identifier=oai:serval.unil.ch:BIB_BE27CB42DEC8)
 

{{% /section %}}

---
{{% section %}}

### MADS

---

#### Intro
* **M**etadata **A**uthority **D**escription **S**tandard
* Abgeleitet von MARC für Autoritätsdaten
* [MADS Official Web Site](http://www.loc.gov/standards/mads/)
* Auch als [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html) publiziert 

<img class="special-img-class" style="width:40%;border:none" src="/images/mads.gif" />

{{% /section %}}