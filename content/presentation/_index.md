+++
outputs = ["Reveal"]
+++
<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

# Bibliothekarische Metadatenformate
  
---

## Intro
* Metadaten und Standards
* Daten beziehen
* Daten analysieren
* Metadaten als Linked Data
* Daten transformieren