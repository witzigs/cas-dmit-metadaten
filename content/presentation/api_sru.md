+++
weight = 320
+++
{{% section %}}

## SRU

---

### Intro
* **S**earch/**R**etrieval via **U**RL
* Protokoll für Suchanfragen im Internet mittels CQL (**C**ontextual **Q**uery **L**anguage)
* Spezifikationen: https://www.loc.gov/standards/sru/
* Offizielle Weiterentwicklung von Z39.50 (ab 2004)
* Basis für Meta-Suchportale (KVK, Archives Online, ...)

---

### Grundprinzipien
* Server: stellt SRU für die Suche zur Verfügung
* Client: schickt Suchanfragen an den Server, nimmt Antworten entgegen und bereitet sie auf
* CQL für Suchanfragen
* Basiert auf XML/HTTP (REST)

---

### [Parameter und Query](/daten_beziehen/sru/requests/)

{{% /section %}}
