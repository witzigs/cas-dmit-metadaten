+++
weight = 330
+++

### Übung
* [Zu OAI-PMH](/daten_beziehen/oai/uebung/)
* [Zu SRU](/daten_beziehen/sru/uebung/)


---

### OAI oder SRU?

|OAI|SRU|
|---|---|
|Harvesting, einmalig oder regelmässig|Suche in externer Quelle und Darstellung in eigenem Kontext
|Rudimentäre Selektionskriterien (sets, datestamp)|Ausgefeilte Suchmöglichkeiten (CQL)
|Abholen von Updates|Adhoc Abfrage, die immer alle Daten zurückliefert


---

### Z39.50
* Netzwerkprotokoll zur Abfrage von Bibliothekssystemen (nicht HTTP!)
* Nur im Bibliothekswesen genutzt
* Entwickelt ab 1984
* Vorgänger von SRU
* Nutzung wie SRU, Suchabfragen auf Server (z.B. Suche in Fremddaten)
* Beispiel: [Alma Z39.50](https://developers.exlibrisgroup.com/alma/integrations/z39-50/)

---

### Systemspezifische Schnittstellen
* Schnittstellen basierend auf Prinzipien von REST, WebHooks, ...
* Nur innerhalb eines Systems standardisiert
* Können auch schreibenden Zugriff ermöglichen
* Ermöglichen Automatisierung von Aufgaben, Anbindung von Drittsystemen, Verändern von Daten, ...
* Beispiel: [Alma REST APIs](https://developers.exlibrisgroup.com/alma/apis/)
