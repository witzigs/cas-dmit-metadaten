+++
weight = 100
+++

{{% section %}}

## Metadaten

Daten über Daten

---

> One feature of metadata that is often forgotten is that it is a human construct and not found in nature. The shape of metadata is designed by human beings for a particular purpose or to solve a particular problem, and the form it takes is indelibly stamped with its origins.  
> There is nothing objective about metadata: it always makes a statement about the world, and this statement is subjective is what it includes, what it omits, where it draws its boundaries and in the terms it uses to describe it.

<div class="citation" style="font-size:50%;text-align:left">
Gartner, Richard. Metadata (Springer, 2016), 4
</div>

---

Metadaten ...

* ... sind von Menschen konstruiert
* ... entstehen in einem Kontext
* ... machen eine subjektive Aussage

---

### Unterschiedliche Erschliessung

<div class="container">

<div class="col">
Autorin des ersten Artikels
<img class="special-img-class" style="width:100%;border:none" src="/images/berio-et-al.png" />
</div>

<div class="col">
Herausgeber
<img class="special-img-class" style="width:100%;border:none" src="/images/berio-hrsg.png" />
</div>

</div>

---

### Abbildung von Themen in Klassifikationen

<div class="container">

<div class="col">
DDC
<img class="special-img-class" style="width:95%;border:none" src="/images/ddc-religion.png" />
</div>

<div class="col">
NDC
<img class="special-img-class" style="width:95%;border:none" src="/images/ndc-religion.png" />
</div>

</div>

---

### Anderes Ziel, anderer Kontext

Archiv

<img class="special-img-class" style="width:100%;border:none" src="/images/kantonsblatt-archiv.png" />

---

### Anderes Ziel, anderer Kontext

Bibliothek

<img class="special-img-class" style="width:70%;border:none" src="/images/kantonsblatt-bibliothek.png" />


{{% /section %}}