+++
weight = 210
+++
{{% section %}}
### MARC

---

#### Intro
* **Ma**chine **R**eadable **C**ataloging
* Aktuell: MARC21
* Library of Congress
* DER Standard im Bibliotheksbereich (trotz "MARC must die"...)
* diverse Varianten

<img class="special-img-class" style="width:15%;border:none" src="/images/marc.png" />

---

#### Geschichte und Entwicklung
* Ab 1966 entwickelt
* LoC, Henriette Avram
* Metadaten maschinenlesbar machen
* Abläufe und Austausch automatisieren
* Katalogkarten und ISBD Interpunktion

---

#### Versionen
* MARC21
* UNIMARC -> IFLA, Frankreich, div. weitere (auch Basis für weitere Versionen)
* Land/Sprachspezifisch (z.B.: CNMARC -> China)
* IDSMARC -> Deutschschweiz, mit SLSP grösstenteils abgelöst durch MARC21
* ...

---

#### Ähnliche Standards
* MAB
* PICA

---

#### MARC21 - 5 Standards
* Bibliographic
* Authority
* Holdings
* Classifications
* Community

---

#### Bibliographic - Struktur
* Leader
* Kontrollfelder
* Datenfelder

---
##### Leader
* Enthält Verarbeitungsinformationen zum Datensatz
* Elemente sind positionsabhängig
* 24 Zeichen
* Keine Indikatoren und Unterfelder

![Leader](/images/marc-leader.png)

---
##### Kontrollfelder
* Feldbereich 00x
* Enthalten codierte Informationen
* Teilweise fixe Länge und positionsabhängig
* Keine Indikatoren und Unterfelder

![Kontrollfelder](/images/marc-controlfields.png)

---

##### Datenfelder
* 3-stellige Feldnummer
* 2 numerische Indikatoren
* Variable Unterfelder
* Wiederholbarkeit (Felder und Unterfelder)

<img class="special-img-class" style="width:90%" src="/images/marc-fields.png" />

---

#### Lokale Datenelemente
* Felder 9xx und x90
* Unterfeld $9

---

#### Dokumentation
* [MARC Standards](https://www.loc.gov/marc/)
* [MARC21 Bibliographic](https://www.loc.gov/marc/bibliographic/)
* Lokale Hilfsmittel (Anleitungen, Feldhilfen, etc.)

---

#### MARC21 Einordnung
* Primär Strukturstandard
* Auch Datenwertstandard (z.B. Sprachcodes)
* Wichtige Austauschstandards: MARCXML, MARC Bandformat (ISO-2709)
* Speicherformate sind systemabhängig, MARC-Serialisierungen als Austauschformate

---

##### MARC Bandformat

![Bandformat](/images/marc-band.png)

--- 

##### MARCXML

[MARCXML Schema](https://www.loc.gov/standards/marcxml//)

<img class="special-img-class" style="width:55%" src="/images/marc-xml.png" />

---

##### Zeilenbasierte Arbeitsformate

![Aleph seq](/images/marc-seq.png)

---

#### Probleme
* Verständlichkeit Feldnummern
* Komplex zum Erlernen
* Sehr unterschiedlich anwendbar
* Ursprüngliche Ausrichtung auf physische Medien
* Entwicklung schwerfällig durch weite Verbreitung und Prozesse für Änderungen

[comment]: <> (* Verknüpfung zwischen Datensätzen: nachträglich eingeführt, initial mit Strings gedacht)
[comment]: <> (* Hauptansetzung, etc.)


{{% /section %}}