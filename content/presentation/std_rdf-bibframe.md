+++
weight = 500
+++

# Metadaten als Linked Data

---

{{% section %}}
## RDF

---

### Intro
* **R**esource **D**escription **F**ramework
* Modell für die Beschreibung von Informationen als Aussagen
* Abstraktes Modell mit diversen Serialisierungen (RDF/XML, Turtle, N-Triples, JSON-LD)
* Zentraler Baustein für Linked Data


---

### Triple (Aussagen)
Ein Triple ist eine Aussage, die ein Subjekt und ein Objekt miteinander in Beziehung setzt.

Die Beziehung ist vom Subjekt zum Objekt gerichtet und wird mit einem Prädikat benannt.

---
![Triple](/images/tolkien-triple.png "image")

---

### Ressourcen und Literale
* Ressourcen sind eindeutig identifizierbar und werden mit einer URI bezeichnet
* Subjekte und Prädikate sind immer Ressourcen
* Literale sind Zeichenketten (Text, Zahlen, ...)
* Objekte können Ressourcen oder Literale sein

---
![Triple](/images/tolkien-triple-uri.png "image")

---

![Triple](/images/tolkien-triple-literal.png "image")

---

### Vokabulare und Ontologien
* Definieren Klassen (Entitäten, Konzepte) und deren hierarchische Struktur
* Definieren Properties (Eigenschaften, Beziehungen) der Klassen
* Halten Definitionen und Constraints (Einschränkungen) fest
* Beispiel [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
* Beispiel [Wikidata P50 (author)](https://www.wikidata.org/wiki/Property:P50)

---

![Triple](/images/tolkien-class.png "image")

---

### Vokabulare und Ontologien

<img class="special-img-class" style="width:60% ; border:none" src="/images/LOV.png" />

[Linked Open Vocabularies](https://lov.linkeddata.es/dataset/lov/)

---

### Vokabulare und Ontologien

* Können von jedem erstellt werden
* Werden in RDF abgebildet
* Fachspezifisch, Domänenspezifisch, Universell
* Bibliotheksbereich: [RDA Element sets](http://www.rdaregistry.info/), [GND Ontology](https://d-nb.info/standards/elementset/gnd#), [BIBFRAME](https://id.loc.gov/ontologies/bibframe.html), [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html)
* Archivbereich: [Records in Context Ontology RiC-O](https://ica-egad.github.io/RiC-O/index.html)
* Weitere: [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), [Wikidata](https://www.wikidata.org/wiki/Wikidata:List_of_properties), [FOAF](http://xmlns.com/foaf/spec/), [Schema.org](https://schema.org/), [PROV-O](https://www.w3.org/TR/prov-o/), [PREMIS](http://www.loc.gov/standards/premis/ontology/index.html), [CIDOC CRM](http://www.cidoc-crm.org/), [SKOS](https://www.w3.org/2009/08/skos-reference/skos.html), ...

---

### Datensets

<img class="special-img-class" style="width:50% ; border:none" src="/images/lod-cloud.jpeg" />

[Linked Open Data Cloud](https://lod-cloud.net/)


---

### Datensets

* Bibliotheken: Bibliografische Daten, Autoritätsdaten (z.B. GND)
* Beispiel: [lobid](https://lobid.org/)
* Allgemein: [Wikidata](https://www.wikidata.org), [DBpedia](https://www.dbpedia.org/), [GeoNames](https://www.geonames.org/) und viele, viele weitere!

{{% /section %}}


---
{{% section %}}
## BIBFRAME

---

### Intro
* **Bib**liographic **Frame**work Initiative
* Ziele: Ablösung von MARC, dabei robustes Austauschformat erhalten
* Diverse Aspekte einbezogen: Datenaustausch, auch ausserhalb der Bibliotheken, Tools und Prozesse für eine Umstellung von MARC zu BIBFRAME, Methoden für die Erschliessung, ...
* Datenmodell und Vokabular nach RDF
* BIBFRAME 1.0 2012, 2016 überarbeitet zu BIBFRAME 2.0

<img class="special-img-class" style="width:15%;border:none" src="/images/bibframe-logo-small.jpeg" />

---

### [BIBFRAME Modell](https://www.loc.gov/bibframe/docs/bibframe2-model.html)

![Bibframe model](/images/bf2-model.jpeg "image")

---

### BIBFRAME Vocabulary
[BIBFRAME Ontology: List view](https://id.loc.gov/ontologies/bibframe.html)

[BIBFRAME Ontology: Category view](https://id.loc.gov/ontologies/bibframe-category.html)

---

### BIBFRAME in der Praxis
**Library of Congress**

* Pilotphase Erschliessung mit BIBFRAME (2017-2020)
* Entwicklung von Mappings, Workflows und [Extensions](https://id.loc.gov/ontologies/bflc.html)
* Entwicklung Transformationen ([MARC zu BIBFRAME](https://www.loc.gov/bibframe/mtbf/), aber auch [BIBFRAME zu MARC](https://www.loc.gov/bibframe/bftm/))
* Entwicklung [Editor](http://bibframe.org/bfe/index.html)
* Publikation von [Datensets](https://id.loc.gov/)
* 2021 Umstieg auf BIBFRAME für die Katalogisierung

---

### BIBFRAME in der Praxis
**Libris (Schweden)**

* Erschliessung in auf BIBFRAME basiertem Vokabular seit 2018
* Entwicklung [Datenmodell](https://id.kb.se/vocab/), "extending BIBFRAME"
* Entwicklung von Transformationen (MARC zu LD und LD zu MARC)
* Entwicklung [Editor](https://libris.kb.se/katalogisering)
* Open Source ([GitHub](https://github.com/libris))

---

### BIBFRAME in der Praxis

**Ex Libris**

* MARC zu BIBFRAME Transformation in Alma integriert für Export
* Erschliessung geplant, vgl. [Alma FAQ](https://knowledge.exlibrisgroup.com/Alma/Product_Materials/050Alma_FAQs/General/Standards#BIBFRAME) und [Developer Network](https://developers.exlibrisgroup.com/alma/integrations/linked_data/bibframe/)

**Weitere**
* Projekte zur Transformation von MARC oder anderen (Intern-)Formaten zu BIBFRAME (DNB, ...)
* Projekte zur Erschliessung: [LD4P](https://wiki.lyrasis.org/pages/viewpage.action?pageId=74515029) Linked Data for Production, [LD4P2](https://wiki.lyrasis.org/display/LD4P2)

{{% /section %}}

---

### Linked Data und Bibliotheken: Fragen über Fragen
* Welcher Fokus? Erschliessung als Linked Data oder Publishing?
* Welcher Standard für die Erschliessung? BIBFRAME mit RDA? Lokale Ergänzungen?
* Welcher Standard für das Publishing? BIBFRAME? schema.org? Lokale Datenmodelle?
* Wie umgehen mit legacy data?
* Wie umgehen mit der Abhängigkeit von Systemen, Workflows, ... zu MARC?