+++
weight = 101
+++

{{% section %}}

### Arten von Metadaten

* Deskriptive Metadaten
* Administrative Metadaten
* Strukturelle Metadaten

---

#### Deskriptive Metadaten
* Finden und identifizieren
* Bibliografische Daten

---

![Ente](/images/deskriptiv-ente.png)

---

#### Administrative Metadaten
* Daten zur Verwaltung von Ressourcen: Einkaufen, Ausleihen, Digitalisieren, ...
* Technische Metadaten
* Rechtliche Metadaten
* Metadaten zur Erhaltung

---

![Lizenzverwaltung](/images/lizenzverwaltung.png)

---

#### Strukturelle Metadaten

* Beziehungen von Teilen
* Strukturierung von Digitalisaten

---

![Struktur Digitalisat](/images/digitalisat-struktur.png)

{{% /section %}}

---

### [Ihre Erfahrungen mit Metadaten](http://www.menti.com/)