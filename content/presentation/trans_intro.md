+++
weight = 600
+++

# Daten transformieren

---

## Ziele

* Daten verbessern (ergänzen, einem neuen Standard anpassen, korrigieren, ...)
* Daten in verschiedenen Formen zur Verfügung stellen (Internformat vs. Austauschformat, LOD-Kontext)
* Daten aus verschiedenen Quellen homogenisieren (Aggregator, Suche ermöglichen)
* Daten indexieren (Dokument für Suchmaschine, Text Analysis, ...)
* Daten für Weiterverarbeitung und Analyse aufbereiten (Zeilenbasierte Formate, CSV, ...)

---

## Hilfsmittel
* Crosswalks zwischen Formaten
* Zur Verfügung gestellte Skripte
* Diverse [Tools](/daten_transformieren/tools/)

---

{{% section %}}

## Herausforderungen

* Verlust an Genauigkeit bei Umwandlung von stark strukturierten Daten in ein weniger strukturiertes Format
* Interpretation und Ungenauigkeit bei Umwandlung von weniger strukturierten Daten in stark strukturierte Daten
* Datenqualität des Datenbestands mit dem zusammengeführt werden soll ist nicht oder nur mit viel Aufwand zu erreichen
* Unterschiedliche Datenqualität in den zu transformierenden Daten

---

### Verlust an Genauigkeit
* Worst case: Datenverlust
* Normalerweise steht mindestens ein Notizfeld zur Verfügung, das mit Prefix verwendet werden kann. Grosser Nachteil: nur noch durch Menschen interpretierbar
* MODS host info ([Beispiel Serval](https://serval.unil.ch/oaiprovider?verb=GetRecord&metadataPrefix=mods&identifier=oai:serval.unil.ch:BIB_35B084C09815)) zu MARC 773 ([Beispiel swissbib](https://www.swissbib.ch/Record/529930021/Details#tabnav))

---
### Interpretation und Ungenauigkeit
Daten müssen interpretiert werden, um sie zu übertragen 

dc:date = 2006

zu MARC 264

* 264 _0 $c = Entstehungsdatum
* 264 _1 $c = Erscheinungsdatum
* 264 _2 $c = Vertriebsdatum
* 264 _3 $c = Herstellungsdatum
* 264 _4 $c = Copyrightdatum

---

### Datenqualität
Daten transformierbar aber entsprechen nicht dem Katalogisierungsstandard

Mikrofilmarchiv
* 33x muss durch Handarbeit ergänzt werden

---

### Datenqualität
Datenset enthält nicht dem Standard entsprechende Daten
* Automatisierte Transformation geht nicht oder muss entsprechend angepasst werden
* In grossen Datenmengen sind Fehler, die nur wenige Daten betreffen leicht zu übersehen
* Gründe: Fehler bei der Erfassung, Altdaten, Kontext, ...

BORIS Sprachcode "me" für mehrsprachig
* 2stellig, andere Codes sind 3stellig
* nach ISO 639-3 wäre "mul" zu verwenden

{{% /section %}}




