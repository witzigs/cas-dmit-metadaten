---
title: "Übung"
date: 2020-09-28T14:59:46+02:00
---

Beantworten Sie für eine OAI-Schnittstelle Ihrer Wahl folgende Fragen:

* Welche Metadatenformate bietet das Repository an?
* Suchen Sie auf der Oberfläche des Repositories nach einer Aufnahme mit dem Stichwort "metadata". Holen Sie die erste Aufnahme auf der Trefferliste über die OAI-Schnittstelle ab. Wann wurde diese Aufnahme zuletzt bearbeitet?
* Welche Sets bietet das Repository an?
* In welchen Sets ist die vorher abgefragte Aufnahme enthalten?
* Welcher Titel wird für Records, die am letzten Montag bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?
* Suchen Sie nach den Records, die in diesem Monat bearbeitet wurden. Holen Sie auch die nächste Seite ab.
* Speichern Sie die ausgegebenen Resultate ab (z.B. mit curl).


### Mögliche OAI-Schnittstellen
* [ZORA](https://www.zora.uzh.ch/cgi/oai2)
* [SERVAL](http://serval.unil.ch/oaiprovider)
* [e-periodica](https://www.e-periodica.ch/oai/dataprovider)
* [RERO doc](http://doc.rero.ch/oai2d)

### Lösungen
<br/>
<details>
  <summary>Welche Metadatenformate bietet das Repository an?</summary>
  <ul>
    <li>Mit einer Abfrage mit dem Request ListMetadataFormats kann diese Frage beantwortet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie auf der Oberfläche des Repositories nach einer Aufnahme mit dem Stichwort "metadata". Holen Sie die erste Aufnahme auf der Trefferliste über die OAI-Schnittstelle ab. Wann wurde diese Aufnahme zuletzt bearbeitet?</summary>
  <ul>
    <li>Es kann helfen, sich die Struktur der Identifier über ListIdentifiers anzuschauen.</li>
    <li>Anschliessend kann die Aufnahme mit dem Request GetRecord abgeholt werden.</li>
    <li>Im Header der Response wird im Tag datestamp das letzte Bearbeitungsdatum ausgegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welche Sets bietet das Repository an?</summary>
  <ul>
    <li>Sets können mit dem Request ListSets aufgelistet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>In welchen Sets ist die vorher abgefragte Aufnahme enthalten?</summary>
  <ul>
    <li>Die Zugehörigkeit eines Records zu einem Set ist im Header im Tag setSpec angegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welcher Titel wird für Records, die am letzten Montag bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?</summary>
  <ul>
    <li>Mit ListRecords und den Argumenten from und until können Records, die in einem bestimmten Zeitraum bearbeitet wurden, abgeholt werden.</li>
    <li>Das Erscheinungsdatum ist in den Metadaten zu finden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie nach den Records, die in diesem Monat bearbeitet wurden. Holen Sie auch die nächste Seite ab.</summary>
  <ul>
    <li>Mit ListRecords und dem Argument from können Records, die seit einem bestimmten Datum bearbeitet wurden, abgeholt werden.</li>
    <li>Die nächste Seite, falls vorhanden, kann mit dem resumptionToken abgeholt werden. Das Token wird am Ende der Response im Tag resumptionToken angegeben</li>
  </ul>
</details>
<br/>
<details>
  <summary>Speichern Sie die ausgegebenen Resultate ab (z.B. mit curl).</summary>
  <ul>
    <li>Zum Beispiel: curl 'https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&set=driver' > results.xml</li>
  </ul>
</details>
<br/>