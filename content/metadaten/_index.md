---
title: "Metadaten"
date: 2020-09-28T17:28:11+02:00
draft: true
---

### Definition und Arten von Metadaten
* deskriptive Metadaten (Ziele: finden, verstehen)
* administrative Metadaten (technisch -> Bild, PDF, ... / rechtliche Aspekte -> CC, Nutzung / Erhaltung, preservation)
* strukturelle Metadaten (Inhaltsverzeichnis, Seitenreihenfolge in einem Buch)

### Standards und Formate im Bibliotheksbereich
inkl. Vor- und Nachteile in der Praxis

* MARC (Unimarc, PICA, MAB)
* METS/MODS
* DublinCore
* Bibframe
* EAD, ISAD-G, RiC -> Blick über den Tellerrand
* nicht standardisiertes, systemspezifisches (Items, Bestellungen, etc.)
* RDF


### Standards für
* for data structures (DC, EAD, MARC21, MODS, ...)
* for data contents (RDA, AACR2, ...)
* for data exchange (ISO2709 für MARC, XML, RDF/XML, JSON, HTML, ...)
* for data values (LCSH, AAT, ...)
(Metadata, S. 23)