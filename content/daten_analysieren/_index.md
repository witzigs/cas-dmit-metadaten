---
title: "Daten analysieren"
date: 2020-10-18T13:24:11+02:00
weight: 20
---
<!--
### 

* Datenset kennenlernen und einschätzen:
    * Wie ist die Datenqualität? 
    * Ist die Datenqualität ausreichend für mein Projekt?
    * Sind zwingend notwendige Elemente enthalten?    
    * Kann ich selbst Verbesserungen an den Daten vornehmen?
    
* Eigene Arbeit überprüfen    

* Kommandozeile: cut, grep, sort, uniq, sed
* CSV und Excel (Filter, Encoding,...)
* XML, Stylesheets
* Elasticsearch und Kibana
* Alma Analytics (andere Analysetools)

### Wie liegen die Daten vor?
Daten umwandeln, um besser damit arbeiten zu können oder entsprechende Tools verwenden

* CSV
* XML, 1 record pro Zeile
* XML, 1 Datenfeld pro Zeile
* Zeilenbasierte Formate wie Aleph sequential
* json
-->
