---
title: "Übung BORIS"
date: 2020-10-18T13:58:11+02:00
---

### Datenset aus BORIS in Dublin Core
Kontext: Das Datenset wurde über die OAI-Schnittstelle von BORIS in Dublin Core exportiert. Die Daten sollen für ein Projekt weiter genutzt werden und müssen dazu in MARC21 (Bibliographic) umgewandelt werden. Dazu müssen die Daten zunächst analysiert werden.

#### Fragen
* Wie viele Aufnahmen sind enthalten?
* Wie oft kommen Sprachangaben vor? 
* Wie sind die Sprachen codiert?
* Welche Sprachcodes kommen vor und welche wie oft?
* Sind diese Sprachcodes alle gültig? Wofür steht der Code "me"?
* Welche Identifier sind in den Daten enthalten?
* Wie kann die PMID (PubMed ID) erkannt werden?
* Erstelle eine Liste aller PMIDs.
* Gibt es PMIDs, die doppelt vorhanden sind?
* Analysieren Sie die Felder dcterms:bibliographicCitation in denen das Wort journal (case insensitive) vorkommt. Welche Informationen sind darin enthalten? Wie sind diese Informationen voneinander abgetrennt?
* Die Informationen aus dcterms:bibliographicCitation sollen ins MARC-Feld 773 ([Hilfestellung für die Übung](/daten_analysieren/773)) übertragen werden. Überlegen Sie sich Regeln nach denen dies möglich wäre und verifizieren Sie diese anhand der Daten.
* Aus welchen Feldern können die für 773 noch zusätzlich notwendigen Informationen extrahiert werden?



#### Tipps
<br/>
<details>
  <summary>Zum Zählen</summary>
  <ul>
    <li>grep hat eine nützliche Option -c. Eine Verkettung von grep und wc -l funktioniert ebenfalls.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zur Auswertung der Sprachcodes</summary>
  <ul>
    <li>Hier ist eine Verkettung von Shell-Befehlen notwendig. Schauen Sie sich grep, cut mit Option -c, sort und uniq mit Option -c an.</li>
  </ul>  
</details>
<br/>
<details>
  <summary>Zum Erstellen der PMID-Liste</summary>
  <ul>
    <li>Hier kann mit grep und cut oder einer Kombination von grep und sed gearbeitet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zum Finden der doppelten PMIDs</summary>
  <ul>
    <li>Hier ist ebenfalls uniq nützlich, aber mit Option -d</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zur Analyse von dcterms:bibliographicCitation</summary>
  <ul>
    <li>grep hat eine Option -i für case insensitive</li>
  </ul>
</details>

#### Lösungen
<br/>
<details>
    <summary>Wie viele Aufnahmen sind enthalten?</summary>
      <ul>
        <li>22924 Aufnahmen</li>
        <li>Auszählung mit grep über den record tag</li>
        <li>grep -c '&lt;record&gt;' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie viele Aufnahmen sind enthalten?</summary>
      <ul>
        <li>23837 Felder dc:language</li>
        <li>Auszählung mit grep über dc:language</li>
        <li>grep -c 'dc:language' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie sind die Sprachen codiert?</summary>
      <ul>
        <li>Mit einem Code aus 2 oder 3 Buchstaben, wahrscheinlich ISO-639-3 (deu vs ger)</li>
      </ul>
</details>
<br/>
<details>
    <summary>Welche Sprachcodes kommen vor und welche wie oft?</summary>
    <div>
        5 arg<br/>
        3209 deu<br/>
        20056 eng<br/>
        297 fra<br/>
        2 hun<br/>
        32 ita<br/>
        2 lao<br/>
        115 me<br/>
        2 pol<br/>
        13 por<br/>
        5 rus<br/>
        1 sh<br/>
        96 spa<br/>
        1 tgk<br/>
        1 zho
   </div>
   <ul>
        <li>grep 'dc:language' boris-export.20201127103438.xml | cut -c 22-24 | sort | uniq -c </li>
   </ul>
</details>
<br/>
<details>
    <summary>Sind diese Sprachcodes alle gültig? Wofür steht der Code "me"?</summary>
      <ul>
        <li>Nein, die Codes sh und me sind nach ISO-639-3 nicht gültig.</li>
        <li>me wird für "Mehrere Sprachen" verwendet. Das lässt sich an einem Beispiel auf der Oberfläche feststellen, z.B.: https://boris.unibe.ch/2132/</li>
      </ul>
</details>
<br/>
<details>
    <summary>Welche identifier sind in den Daten enthalten?</summary>
      <ul>
        <li>DOI, PMID, ISSN, BORIS URL, String mit Zitat</li>
        <li>Auswertung des Felds dc:identifier</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie kann die PMID erkannt werden?</summary>
      <ul>
        <li>Durch den prefix info:pmid im Feld dc:identifier</li>
      </ul>
</details>
<br/>
<details>
    <summary>Erstelle eine Liste aller PMIDs.</summary>
    <ul>
        <li>grep 'dc:identifier&gt;info:pmid' boris-export.20201127103438.xml > boris-pmid.csv</li>
    </ul>
</details>
<br/>
<details>
    <summary>Gibt es PMIDs, die doppelt vorhanden sind?</summary>
      <ul>
        <li>Ja, PMID 31536527 kommt zwei Mal in den Daten vor.</li>
        <li>sort boris-pmid.csv | uniq -d</li>
      </ul>
</details>
<br/>
<details>
    <summary>Analysieren Sie das Feld dcterms:bibliographicCitation in denen das Wort journal (case insensitive) vorkommt? Welche Informationen sind darin enthalten? Wie sind diese Informationen voneinander abgetrennt?</summary>
      <ul>
        <li>Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Verlag</li>
        <li>Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag</li>
        <li>Zeitschriftentitel, Volumenummer, Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag</li>
        <li>grep -i 'dcterms:bibliographicCitation' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Die Informationen aus dcterms:bibliographicCitation sollen ins MARC-Feld 773 übertragen werden. Überlegen Sie sich Regeln nach denen dies möglich wäre und verifizieren Sie diese anhand der Daten.</summary>
      <ul>
        <li>Als Trennzeichen zwischen Titel und Inhalten für 773 $g: Komma Spatium Zahl</li>
        <li>Als Trennzeichen zwischen Seitenzahlen und Ort/Verlag: Zahl Punkt Spatium</li>
      </ul>
</details>
<br/>
<details>
    <summary>Aus welchen Feldern können die für 773 noch zusätzlich notwendigen Informationen extrahiert werden?</summary>
      <ul>
        <li>Die ISSN aus dc:identifier für 773 $x</li>
        <li>Das Erscheinungsjahr aus dc:date für 773 $g</li>
      </ul>
</details>