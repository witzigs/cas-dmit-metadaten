---
title: "Formate"
date: 2020-09-22T20:16:08+02:00
draft: true
---

### Kategorisierung
* Internformate (PICA+, swissbib Index, ...)
* Erfassungsformate (MARC, ...)
* Präsentationsformate
* Austauschformate (MARC)

Formate können alles sein (MARC = Intern, Erfassung, Austausch)

Alle standardisiert, aber vielleicht nur in einem System... abhängig vom Ziel: Austauschformat muss natürlich stark standardisiert sein

### Systemabhängige Formate
* Beispiel: Exemplare, Erwerbungsdaten, Benutzerdaten, ...
* Ergänzen von bibliografischer Aufnahme durch expands mit Exemplardaten, Holdings, AUT-Formen, ...  -> Informationsverlust, je nach Anwendungszweck zu definieren

### Formate in angrenzenden Bereichen
* Buchhandel
* Literaturverwaltung
