---
title: "Dublin Core"
date: 2020-09-22T20:30:41+02:00
---

https://www.dublincore.org/specifications/dublin-core/dcmi-terms/

* elements: 15 Elemente
* terms: zusätzliche Elemente
* elements und terms als RDF Vokabulare publiziert
* kann auch in XML, JSON, relationalen Datenbanken, ... verwendet werden

### Elements
* title
* description
* type
* subject
* source
* relation
* coverage
* creator
* publisher
* rights
* contributor
* date
* format
* identifier
* language


### Entwicklung
Workshop 1995
* Ziel: bessere Beschreibung von Ressourcen im Internet für Verbesserung der Auffindbarkeit. Anwendbar ohne Expertenwissen, damit möglichst viel beschrieben werden kann.
* Kernelemente / Core Set / Kernfelder / 
* “Can a simple metadata format be defined that sufficiently describes a wide range of electronic objects?” (Metadata, S. 42)
* 15 Core Elemments (DCMES), interdisziplinär, basic elements für Auffindbarkeit, nutzbar für diverse Ressourcen (physisch oder digital)
* Alle Elemente sind optional, wiederholbar, Reihenfolge ist egal
* für bestimmte Elemente Vorgaben für Inhalt (ISO 639-2 für Sprache, eigenes Vokabular für Types
* 2001 Erweiterung mit qualifier (Qualified DC und Simple DC)
* 2003 qualifier überführt in dc terms als eigene Elemente
* dc terms 2003 als RDF publiziert, enthält 15 Elemente plus qualifier -> DCMI Metadata Terms
* Constraints eingeführt 
* DC 1995: Metadata Records werden als kleinste Einheit erstellt
* RDF: Metadata statements werden erstellt / sind die kleinste Einheit


### Schwierigkeiten
* Elemente so breit definiert, dass tlw. unklar wird was gemeint ist (creator)
* Gelöst mit qualifier (vorgegebenes Set von DC, aber auch frei erweiterbar)
* Vorteil an qualifier: wenn man alles nach dem Punkt ignoriert, hat man weiterhin simple dc

### Anwendungsbereiche

### Beispiele
* OA-Repositories


