---
title: "MARC"
date: 2020-09-22T20:30:00+02:00
---
http://www.loc.gov/marc/

* MAchine Readable Cataloging
* LOC -> Katalog(karten) elektronisch teilen/austauschen (LOC als Dienstleister für Katalogkarten in den USA)
* Henriette Avram
* Detail-Analyse Katalogkarten, jedes Element in drei Teile unterteilt: Name, Handling instructions, logische Unterteilung
* Felder mit Zahlen, da sprachunabhängig, weniger fehleranfällig und für Maschinen einfacher zu verarbeiten
* Basis für Union catalogues, WorldCat, … bis heute

### Daten
* für bibliographische Daten
* für Autoritätsdaten
* für Holdings
* für Klassifikationen


### Versionen
* MARC21
* UNIMARC -> Frankreich, div. weiter (auch Basis für weitere Versionen)
* NORMARC -> Norwegen
* CMARC -> China
* IDSMARC -> Deutschschweiz, mit SLSP vollständig abgelöst durch MARC21
* …